/* globals $, QiSession */

var basicAwareness = false
var behaviourManager = false
var connectionManager = false
var settingsManager = false
var audioManager = false
var speechManager = false
var batteryManager = false
var tabletManager = false

var connecting = false

var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart')
var currentVolume = 100

function getServicesJSON(services) {
  return services.map(function (service) {
    return array_reduce(service)
  })
}

function array_reduce(arr) {
  return arr.reduce(function (obj, field) {
    obj[field[0]] = Array.isArray(field[1]) ? array_reduce(field[1]) : field[1]
    return obj
  }, {})
}

$(document).ready(function () {
  new QiSession(function (session) {
    connected(session)
  }, disconnected)
  
  $('#network-toggle').on(clickEventType, function () {
    if ($(this).attr('data-state') === 'wifi') {
      setTechnology('tethering')
	  $(this).attr('src', 'images/toggle_on.png')
    } else {
      setTechnology('wifi')
	  $(this).attr('src', 'images/toggle_off.png')
    }
  })

  $('#exit').on(clickEventType, function () {
    if (!behaviourManager) return
    behaviourManager.stopBehavior('User/settings/behavior_1')
  })

  $('.bar').each(function () {
    $(this).on(clickEventType, function (evt) {
      currentVolume = parseInt($(this).attr('id').split('_')[1])
      audioManager.setOutputVolume(currentVolume).then(function () {
        audioManager.muteAudioOut(false).then(function () {
          updateVolume()
        })
      })
    })
  })

  $('#toggle-awareness').on(clickEventType, function () {
    if (!basicAwareness) return

    if ($(this).attr('src') === 'images/toggle_on.png') {
      basicAwareness.stopAwareness()
      $(this).attr('src', 'images/toggle_off.png')
    } else {
      basicAwareness.startAwareness()
      $(this).attr('src', 'images/toggle_on.png')
    }
  })

  $('#toggle-rotation').on(clickEventType, function () {
    if (!basicAwareness) return

    if ($(this).attr('src') === 'images/toggle_on.png') {
      basicAwareness.setTrackingMode('Head')
      $(this).attr('src', 'images/toggle_off.png')
    } else {
      basicAwareness.setTrackingMode('BodyRotation')
      $(this).attr('src', 'images/toggle_on.png')
    }
  })
  
  $('#speaker').on(clickEventType, function (evt) {
    if (!speechManager) return
    speechManager.say('Is this better?')
  })

  $('#brightness-plus').on(clickEventType, function (evt) {
    if (!tabletManager) return
    tabletManager.getBrightness().then(function (current) {
      var adjusted = Math.min(1, current + 0.1)
      tabletManager.setBrightness(adjusted)
      $('#brightness-value').html(Math.round(adjusted * 100))
    })
  })

  $('#brightness-minus').on(clickEventType, function (evt) {
    if (!tabletManager) return
    tabletManager.getBrightness().then(function (current) {
      var adjusted = Math.max(0, current - 0.1)
      tabletManager.setBrightness(adjusted)
      $('#brightness-value').html(Math.round(adjusted * 100))
    })
  })

  $('#save').on(clickEventType, function () {
    $('#loading').show()
    $('#content').hide()

    var options = {
      'ALBasicAwareness.setEnabled': ($('#toggle-awareness').attr('src') === 'images/toggle_on.png') ? 1 : 0,
      'ALBasicAwareness.SetTrackingMode': ($('#toggle-rotation').attr('src') === 'images/toggle_on.png') ? 'BodyRotation' : 'Head'
    }

    settingsManager.save(options).then(function () {
      setTimeout(function () {
        $('#loading').hide()
        $('#content').show()
      }, 500)
    })
  })
})

function connected (session) {
  console.log('Connected')

  session.service('ALBehaviorManager').then(function (service) {
    behaviourManager = service
  })
  session.service('SettingsManager').then(function (service) {
    settingsManager = service
  })
  session.service('ALTextToSpeech').then(function (service) {
    speechManager = service
  })
  session.service('ALAudioDevice').then(function (service) {
    audioManager = service
    updateVolume()
  })

  session.service('ALBattery').then(function (service) {
    batteryManager = service
    updateBattery()
  })

  session.service('ALMemory').then(function (service) {
    service.subscriber('BatteryChargeChanged').then(function (subscriber) {
      subscriber.signal.connect(function () {
        updateBattery()
      })
    })
  })

  session.service('ALTabletService').then(function (service) {
    tabletManager = service
    tabletManager.getBrightness().then(function (current) {
      $('#brightness-value').html(Math.round(current * 100))
    })
  })

  session.service('ALConnectionManager').then(function (connman) {
    connectionManager = connman
    connectionManager.services().then(function (services) {
      services = services.filter(function (s) {
        return s[3][1] === 'online'
      })
      if (services) {
        $('#network-ip').html('Connected: ' + services[0][9][1][1][1])
      } else {
        $('#network-ip').html('Disconnected')
      }
    })
  })
  
  session.service('ALMemory').then(function (service) {
    service.subscriber('NetworkStateChanged').then(function (subscriber) {
      subscriber.signal.connect(function () {
        update()
      })
    })
    service.subscriber('NetworkConnectStatus').then(function (subscriber) {
      subscriber.signal.connect(function (status) {
        if (!status[1]) {
          //speechManager.say(status[2])
        }
        connecting = false
        update()
      })
    })
  })

  session.service('ALBasicAwareness').then(function (service) {
    basicAwareness = service

    var pending = []

    pending.push(basicAwareness.isEnabled())
    pending.push(basicAwareness.getTrackingMode())

    $.when.apply($, pending).then(function (enabled, trackingMode) {
      if (!enabled) {
        $('#toggle-awareness').attr('src', 'images/toggle_off.png')
      }
      if (trackingMode === 'Head') {
        $('#toggle-rotation').attr('src', 'images/toggle_off.png')
      }
      $('#loading').hide()
      $('#content').show()
    })
  })
}

function disconnected (session) {
  console.log('Disconnected')
}

function updateBattery () {
  if (!batteryManager) {
    return
  }
  batteryManager.getBatteryCharge().then(function (percent) {
    $('#battery-text').html(percent.toString() + '%')
    $('#battery-left').attr('offset', percent.toString() + '%')
    $('#battery-right').attr('offset', percent.toString() + '%')
  })
}

function updateVolume () {
  if (!audioManager) return

  audioManager.getOutputVolume().then(function (volume) {
    currentVolume = volume
    draw()
  })
}

function draw () {
  for (var i = 0; i < 100; i += 5) {
    if (currentVolume <= i) {
      $('#vol_' + (i + 5).toString()).removeClass('filled')
    } else {
      $('#vol_' + (i + 5).toString()).addClass('filled')
    }
  }
}

function update () {
  if (!connectionManager) {
    return
  }
  
  connectionManager.getTetheringEnable('wifi').then(function (result) {
    if (result) {
      $('#network-toggle').attr('data-state', 'tethering')
      //$('#network-toggle').attr('src', 'images/toggle_right.png')
    } else {
      $('#network-toggle').attr('data-state', 'wifi')
      //$('#network-toggle').attr('src', 'images/toggle_left.png')
    }
  })

  connectionManager.services().then(function (services) {
    services = getServicesJSON(services)
    
    var current = undefined;
    for (i = 0; i < services.length; i++) {
      var service = services[i]
      
      if (service.Type === 'wifi' && (service.State === 'online' || service.State == 'ready')) {
        current = service
        break
      }
    }
    
    if (current != undefined) {
      $('#network-ip').html(current.IPv4.Address)
      $('.loading').hide()
      
    } else {
      connectionManager.getTetheringEnable('wifi').then(function (enabled) {
        if (enabled) {
          $('#network-ip').html('192.168.0.1')
        } else {
          if (connecting) {
            $('#network-ip').html('Connecting')
          } else {
            $('#network-ip').html('Disconnected')
          }
        }
        $('.loading').hide()
      })
    }
  }).catch(function (err) {
    console.log(err)
  })
}

function setTechnology(technology) {
  if (technology === 'tethering') {
    enableTethering()
  } else {
    enableWiFi()
  }
  $('.loading').show()
}

function enableTethering() {
  connectionManager.enableTethering('wifi', 'pepper', 'cyphy123').then(function () {
    connectionManager.getTetheringEnable('wifi').then(function (enabled) {
      if (enabled) {
        //speechManager.say('Tethering is enabled')
        update()
      }
    })
  })
}

function disableTethering () {
  return new Promise(function (resolve, reject) {
    connectionManager.disableTethering('wifi').then(function () {
      connectionManager.getTetheringEnable('wifi').then(function (enabled) {
        if (!enabled) {
          resolve()
        } else {
          reject()
        }
      })
    })
  })
}

function enableWiFi() {
  disableTethering().then(function () {
    connectionManager.scan('wifi').then(function () {
      connectionManager.services().then(function (services) {
        services = getServicesJSON(services)

        for (i = 0; i < services.length; i++) {
          var service = services[i]

          if (service.Type != 'wifi') {
            continue
          }

          if (!service.Favorite) {
            continue
          }

          connectionManager.connect(service.ServiceId).then(function () {
            //speechManager.say('WiFi is enabled')
            connecting = true
            update()
          })
          break
        }
      })
    }).catch(function (err) {
      console.log(err)
      enableWiFi()
    })
  }).catch(function (err) {
    enableWiFi()
  })
}