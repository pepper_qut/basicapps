<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Module installed. Please reboot me.</source>
            <comment>Text</comment>
            <translation type="obsolete">Module installed. Please reboot me.</translation>
        </message>
    </context>
</TS>
