<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Walkies" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="Thumbs" src="Thumbs.db" />
        <File name="libmotion_controller" src="libs/libmotion_controller.so" />
        <File name="settings_manager" src="behavior_1/scripts/settings_manager.py" />
        <File name="style" src="html/css/style.css" />
        <File name="index" src="html/index.html" />
        <File name="WalkiesIcon" src="WalkiesIcon.png" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="walking" src="html/images/walking.png" />
        <File name="readme" src="html/readme.html" />
        <File name="colorschememapping" src="html/readme_files/colorschememapping.xml" />
        <File name="filelist" src="html/readme_files/filelist.xml" />
        <File name="image001" src="html/readme_files/image001.png" />
        <File name="image002" src="html/readme_files/image002.png" />
        <File name="image003" src="html/readme_files/image003.jpg" />
        <File name="image004" src="html/readme_files/image004.jpg" />
        <File name="themedata" src="html/readme_files/themedata.thmx" />
        <File name="BlinkingPepper" src="html/images/BlinkingPepper.gif" />
        <File name="back" src="html/images/back.png" />
        <File name="forward" src="html/images/forward.png" />
        <File name="left" src="html/images/left.png" />
        <File name="leftTurn" src="html/images/leftTurn.png" />
        <File name="quit" src="html/images/quit.png" />
        <File name="right" src="html/images/right.png" />
        <File name="rightTurn" src="html/images/rightTurn.png" />
        <File name="logo" src="html/images/logo.png" />
        <File name="logo2" src="html/images/logo2.png" />
        <File name="CHFlogo" src="html/images/CHFlogo.png" />
        <File name="CHF-LogoStacked" src="html/images/CHF-LogoStacked.jpg" />
        <File name="eventLogo" src="html/images/eventLogo.png" />
        <File name="indexCHF" src="html/indexCHF.html" />
        <File name="AQLogo" src="html/images/AQLogo.PNG" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
