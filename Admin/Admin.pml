<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Admin" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="PIN" src="html/PIN.txt" />
        <File name="style" src="html/css/style.css" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="pin" src="html/pin.html" />
    </Resources>
    <Topics />
    <IgnoredPaths>
        <Path src="html/images" />
        <Path src="html/images/Thumbs.db" />
        <Path src="translations" />
        <Path src="translations/translation_en_US.ts" />
    </IgnoredPaths>
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
