/****************************************************/
/* Aldebaran Behavior Complementary Development Kit */
/* ConnectionHtmlChoregraphe: Connection.js         */
/* Innovation - Protolab - mcaniot@aldebaran.com    */
/* Aldebaran Robotics (c) 2016 All Rights Reserved. */
/* This file is confidential.                       */
/* NOTE: THIS FILE IS PUBLISHED ONLINE               */
/****************************************************/

/* Create a session for the connection */
var session = new QiSession();
var vid;

/* This function allow to connect with ALMemory thank to the box "Raise Event".
You need to give the key "PepperQiMessaging/totablet"*/
function startSubscribe() {
	session.service("ALTextToSpeech").done(function (service) {
		speechService = service;
		speechService.setParameter("speed",85);
    });
    session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("PepperQiMessaging/totablet").done(function(subscriber) {
            subscriber.signal.connect(toTabletHandler);
        });    
		ALMemory.subscriber("topicChosen").done(function(subscriber) {
            subscriber.signal.connect(showPopup);
        });    
        ALMemory.subscriber("ALAnimatedSpeech/EndOfAnimatedSpeech").done(function(subscriber) {
            subscriber.signal.connect(endSpeech);
        });   
    });
}

function setVideo() {
	//vid = document.getElementById('video1');
	//vid.play();
}

/* Receive the data send by choregraphe with the id "command". 
You can change the name of the id.*/ 
function toTabletHandler(value) { 
    // get the data and put it in the id "command"
    //document.getElementById("count").value= value;
    //tmp = document.getElementById("command").value;
    // send the data to html page
    //document.getElementById("count").innerHTML=value;
	//int count = parseInt(value);	
	//document.getElementById("debug").innerHTML = String(value);
	if (value>0) {
		document.getElementById("count1").style.background = '#03E8FF';
	} else {
		document.getElementById("count1").style.background = '#000000';
	}
	if (value>1) {
		document.getElementById("count2").style.background = '#03E8FF';
	} else {
		document.getElementById("count2").style.background = '#000000';
	}
	if (value>2) {
		document.getElementById("count3").style.background = '#03E8FF';
	} else {
		document.getElementById("count3").style.background = '#000000';
	}
	if (value>3) {
		document.getElementById("count4").style.background = '#03E8FF';
	} else {
		document.getElementById("count4").style.background = '#000000';
	}
	if (value>4) {
		document.getElementById("count5").style.background = '#03E8FF';
	} else {
		document.getElementById("count5").style.background = '#000000';
	}
    // process data with the function choice()
}

/* Send information to choregraphe thank to the event "PepperQiMessaging/fromtablet".
You need to create this event in choregraphe (add event from ALMemory).*/
function sendToChoregraphe(response) {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/fromTabletResponse", response);
    });
}
function interruptSpeech() {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/interruptSpeech", 1);
    });
	session.service("ALBehaviorManager").done(function (service) {
		service.stopBehavior("donothing/behavior_1");
    });
}
function exitApp() {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/exitApp", 1);
    });
}
function showPopup(topic) {
	var pop = document.getElementById("popupBackground");
	if (topic=="silence") {
		pop.style.backgroundImage = "url('images/silence.png')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="intro") {
		pop.style.backgroundImage = "url('images/checkIn.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="about") {
		pop.style.backgroundImage = "url('images/aboutMe.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="publicHealth") {
		pop.style.backgroundImage = "url('images/publicHealth.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="vision") {
		pop.style.backgroundImage = "url('images/vision.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="language") {
		pop.style.backgroundImage = "url('images/starwars.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="movement") {
		pop.style.backgroundImage = "url('images/moving.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="stress") {
		pop.style.backgroundImage = "url('images/stressed.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="grasping") {
		pop.style.backgroundImage = "url('images/teddy.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	} else if (topic=="dance") {
		pop.style.backgroundImage = "url('images/Robotronica.jpg')";
		pop.style.backgroundSize = "contain";
		pop.style.backgroundRepeat = "no-repeat";
		//document.getElementById('video1')
		//vid.style.display = "none";
	//} else {
		//if (topic=="grasping") {
			//vid.currentTime = 30;
		//} else {
			//vid.currentTime = 0;
		//}
		//pop.style.backgroundImage = "none";
		//vid = document.getElementById('video1');
		//vid.style.display = "block";
		//vid.play();
	}
	popupElement = document.getElementById("popup");
	popupElement.style.display = "block";
}
function endSpeech() {
	popupElement = document.getElementById("popup");
	popupElement.style.display = "none";
	//vid.pause();
}
