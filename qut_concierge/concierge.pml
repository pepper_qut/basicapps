<?xml version="1.0" encoding="UTF-8" ?>
<Package name="THHS" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="QUTconcierge" src="QUTconcierge/QUTconcierge.dlg" />
    </Dialogs>
    <Resources>
        <File name="README" src="README.md" />
        <File name="Connection" src="html/css/Connection.css" />
        <File name="style" src="html/css/style.css" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="icon" src="icon.png" />
        <File name="choice_sentences_light" src="behavior_1/Aldebaran/choice_sentences_light.xml" />
        <File name="index" src="html/index.html" />
        <File name="badnames" src="html/js/badnames.js" />
        <File name="badwords" src="html/js/badwords.js" />
        <File name="About" src="html/images/About.png" />
        <File name="Apple" src="html/images/Apple.png" />
        <File name="Dance" src="html/images/Dance.png" />
        <File name="Eye" src="html/images/Eye.png" />
        <File name="Hands" src="html/images/Hands.png" />
        <File name="Navigation" src="html/images/Navigation.png" />
        <File name="QUTlogo" src="html/images/QUTlogo.png" />
        <File name="Vaccination" src="html/images/Vaccination.png" />
        <File name="Vision" src="html/images/Vision.png" />
        <File name="Worried" src="html/images/Worried.png" />
        <File name="rv_logo_colour" src="html/images/rv_logo_colour.png" />
        <File name="silence" src="html/images/silence.png" />
        <File name="vision" src="html/images/vision.jpg" />
        <File name="indexOLD" src="html/indexOLD.html" />
        <File name="checkIn" src="html/images/checkIn.jpg" />
        <File name="publicHealth" src="html/images/publicHealth.jpg" />
        <File name="aboutMe" src="html/images/aboutMe.jpg" />
        <File name="moving" src="html/images/moving.jpg" />
        <File name="stressed" src="html/images/stressed.jpg" />
        <File name="teddy" src="html/images/teddy.jpg" />
        <File name="Robotronica" src="html/images/Robotronica.jpg" />
        <File name="pepperSmart" src="html/images/pepperSmart.jpg" />
        <File name="starwars" src="html/images/starwars.jpg" />
        <File name="Conversation" src="html/images/Conversation.png" />
    </Resources>
    <Topics>
        <Topic name="QUTconcierge_enu" src="QUTconcierge/QUTconcierge_enu.top" topicName="QUTconcierge" language="en_US" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
