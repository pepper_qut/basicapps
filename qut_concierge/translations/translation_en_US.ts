<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Cafe</name>
        <message>
            <source>to get to the Medilink complex, exit by
pressing the green button near the doors. Walk straight
to the adjoining corridor and turn right, head directly
to the main corridor. At the main corridor, turn right
again to head to the shops. You will also find a café on
the first floor of the hospital, above the main hospital
entrance.</source>
            <comment>Text</comment>
            <translation type="obsolete">to get to the Medilink complex, exit by
pressing the green button near the doors. Walk straight
to the adjoining corridor and turn right, head directly
to the main corridor. At the main corridor, turn right
again to head to the shops. You will also find a café on
the first floor of the hospital, above the main hospital
entrance.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Exit</name>
        <message>
            <source>To exit, press the green button on the wall to
your left. To go to the carpark please walk to the
corridor straight ahead and turn left. The double glass
doors open into the garden opposite the main carpark.
Thanks for visiting.</source>
            <comment>Text</comment>
            <translation type="obsolete">To exit, press the green button on the wall to
your left. To go to the carpark please walk to the
corridor straight ahead and turn left. The double glass
doors open into the garden opposite the main carpark.
Thanks for visiting.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Mute</name>
        <message>
            <source>Shh</source>
            <comment>Text</comment>
            <translation type="obsolete">Shh</translation>
        </message>
        <message>
            <source>Oh</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Visit</name>
        <message>
            <source>Hi, the visiting hours for all wards throughout
the hospital are from 8 am. to 8 pm.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi, the visiting hours for all wards throughout
the hospital are from 8 am. to 8 pm.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Visiting</name>
        <message>
            <source>Hi, the visiting hours for all wards throughout
the hospital are from 8 am. to 8 pm.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hi, the visiting hours for all wards throughout
the hospital are from 8 am. to 8 pm.</translation>
        </message>
    </context>
</TS>
